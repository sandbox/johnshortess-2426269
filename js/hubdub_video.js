/**
 * @file
 * Embeds a HubDub video player.
 */

(function ($) {

  $(document).ready(function() {
    var media = new Array();

    for (var setting_key in Drupal.settings) {
      if (setting_key.search('hubdub') != -1) {
        if (video_extension != "youtube") {
          var video_title = Drupal.settings[setting_key]['hubdub_video']['title'];
          var video_description = Drupal.settings[setting_key]['hubdub_video']['description'];
          var video_url = Drupal.settings[setting_key]['hubdub_video']['url'];
          var video_id = Drupal.settings[setting_key]['hubdub_video']['id'];
          var video_ga_enable = Drupal.settings[setting_key]['hubdub_video']['ga_enable'];
          var video_overlays = Drupal.settings[setting_key]['overlays'];
          var video_container_selector = '#' + video_id;
          var video_extension = get_video_extension(video_url);
          var video_swf_path = Drupal.settings[setting_key]['hubdub_video']['swf_path'];
          var video_options = Drupal.settings[setting_key]['hubdub_video']['data'];
          var media = new Object();
          media[video_extension] = video_url;
          media.title = video_title;
          media.video_extension = video_extension;
          instantiate_player(video_container_selector + '-container', video_container_selector + '-player', video_swf_path, media, video_options, video_overlays);
        }
      }
    }

    function display_overlay(overlay) {
      if ($('#' + overlay.id).is(':hidden')) {
        $('#' + overlay.id).show();
        if (video_ga_enable) {
          // _gaq.push(['_trackEvent', 'HubDub', 'OverlayShow', video_id, overlay.id]);
          ga('send', 'event', 'HubDub', 'OverlayShow' , overlay.id, 0);
        }
      }
    }

    function hide_overlay(overlay) {
      if ($('#' + overlay.id).is(':visible')) {
        $('#' + overlay.id).hide();
        if (video_ga_enable) {
          // _gaq.push(['_trackEvent', 'HubDub', 'OverlayHide', video_id, overlay.id]);
          ga('send', 'event', 'HubDub', 'OverlayHide', overlay.id, 0);
        }
      }
    }

    function get_video_extension(video_url) {
      var videoType = "";
      if (video_url.search("rtmp://") != -1) {
        videoType = "rtmpv";
      }
      else if (video_url.search("youtube.com/") != -1) {
        videoType = "youtube";
      }
      else if (video_url.search("mp4") != -1 || video_url.search("m4v") != -1) {
        videoType = "m4v";
      }
      else if (video_url.search("ogv") != -1) {
        videoType = "ogg";
      }
      else if (video_url.search("webm") != -1 || video_url.search("webmv") != -1) {
        videoType = "webm";
      }
      return videoType;
    }

    function reset_all_overlays(media_overlays) {
      for (var overlay_key in media_overlays) {
        var overlay = media_overlays[overlay_key];
        hide_overlay(overlay);
      }
    }

    // Load the video into the target container and player.
    function instantiate_player(jp_container_id, jp_player_id, swf_path, media_file, options, media_overlays) {
      var container_width = $(jp_container_id).innerWidth();
      var container_height = (container_width * 9) / 16;
      var video_player = $(jp_player_id);

      video_player.jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", media_file);
          if (options.autostart) {
            $(this).jPlayer("play");
          }
        },
        swfPath: swf_path,
        supplied: media_file.video_extension,
        size: { width: container_width + "px", height: container_height + "px", cssClass: "jp-video-no-limit" },
        useStateClassSkin: true,
          smoothPlayBar: Boolean(options.smoothPlayBar),
          keyEnabled: Boolean(options.keyEnabled),
          cssSelectorAncestor: jp_container_id,
          autoBlur: Boolean(options.autoBlur),
          warningAlerts: Boolean(options.warningAlerts),
          consoleAlerts: Boolean(options.consoleAlerts),
          errorAlerts: Boolean(options.errorAlerts),
          loop: Boolean(options.loop),
          preload: options.preload,
          backgroundColor: options.backgroundColor,
        autohide: { restored: options.autoHide.restored, full: options.autoHide.full, fadeIn: options.autoHide.fadeIn, fadeOut: options.autoHide.fadeOut, hold: options.autoHide.hold },
      });

      // Set up overlay triggers.
      if (media_overlays) {
        // jPlayer fires off a timeupdate event every quarter-second to
        // half-second, depending on browser type.
        // This event callback toggles display of the overlays depending on
        // where we are in the video.
        video_player.bind($.jPlayer.event.timeupdate, function(event) {
          var current_time = event.jPlayer.status.currentTime;
          for (var overlay_key in media_overlays) {
            var overlay = media_overlays[overlay_key];
            var start_time = overlay.start_time;
            var end_time = overlay.end_time;

            if (end_time != -1) {
              // If this is not a persistent overlay, check start and end time
              // brackets.
              if (current_time >= start_time && current_time < end_time) {
                display_overlay(overlay);
              }
              else {
                hide_overlay(overlay);
              }
            }
            else if (current_time >= start_time) {
              display_overlay(overlay);
            }
            else if (current_time > 0) {
              hide_overlay(overlay);
            }
          }
        });

          function pause_video_player(event) {
            video_player.jPlayer('pause');
          }

          jQuery('.pause-on-focus').mouseenter(pause_video_player);
          jQuery('.pause-on-focus').focusin(pause_video_player);

          video_player.bind($.jPlayer.event.play, function(event) {
            if (video_ga_enable) {
              ga('send', 'event', 'HubDub', 'Play', video_id, event.jPlayer.status.currentTime);
            }
            if (event.jPlayer.status.currentTime == 0) {
              reset_all_overlays(media_overlays);
            }
          });

          video_player.bind($.jPlayer.event.pause, function(event) {
            if (video_ga_enable) {
              ga('send', 'event', 'HubDub', 'Pause', video_id, event.jPlayer.status.currentTime);
            }
          });

          jQuery(window).bind("resize", function() {
            if (jQuery(jp_container_id).hasClass('jp-video-full')) {
              return;
            }

            var container_width = $(jp_container_id).parent().innerWidth();
            var container_height = (container_width * 9) / 16;

            var container = jQuery(jp_container_id);
            var player = jQuery(jp_player_id);
            var video = jQuery(jp_container_id + " video");
            var flash = jQuery(jp_container_id + " object");

            container.width(container_width + "px");
            container.height(container_height + "px");
            player.width(container_width + "px");
            player.height(container_height + "px");
            flash.width(container_width + "px");
            flash.height(container_height + "px");
            video.width(container_width + "px");
            video.height(container_height + "px");
          });
        }
      }
    });
})(jQuery);
