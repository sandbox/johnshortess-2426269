    function is_youtube_video(video_url) {
      if(video_url.search("youtube.com/") != -1)
        return true;
      return false;
    }

    function get_youtube_id(video_url) {
      var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
      var match = video_url.match(regExp);
      if(match && match[7].length==11){
        return match[7];
      }
    }
    
    function display_overlay(overlay) {
      jQuery('#' + overlay.id).show();
    }

    function hide_overlay(overlay) {
      jQuery('#' + overlay.id).hide();
    }

    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    function onYouTubeIframeAPIReady() {
      var media = new Array(); 
      for(var setting_key in Drupal.settings) {
        if(setting_key.search('hubdub') != -1) {
          var video_url = Drupal.settings[setting_key]['hubdub_video']['url'];
          if(is_youtube_video(video_url)) {
            var video_title = Drupal.settings[setting_key]['hubdub_video']['title'];
            var video_description = Drupal.settings[setting_key]['hubdub_video']['description'];
            var video_id = Drupal.settings[setting_key]['hubdub_video']['id'];  
            var video_overlays = Drupal.settings[setting_key]['overlays'];
            instantiateYoutubePlayer(video_id, get_youtube_id(video_url), video_overlays);
          }
        }
      }
    }

    function instantiateYoutubePlayer(video_id, youtube_id, video_overlays) {
      var yt_player = new YT.Player(video_id + '-player', {
        height: '390',
        width: '640',
        videoId: youtube_id,
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        },
        playerVars: {
          autoplay: 0,
          controls: 0,
          enablejsapi: 1,
          modestbranding: 1,
          rel: 0,
          showinfo: 0,
          playlist: youtube_id
        }
      });
      //yt_player.playVideo();
      //yt_player.pauseVideo();

      var duration = 0;  // This can only get set to yt_player.getDuration() once the video playback is initiated; otherwise, an error is generated

      var container_id = '#' + video_id + '-container';
      var player_id = '#' + video_id + '-player';            

      // Add the youtube-video class so that we can hide the controls by default until the video has been interacted with
      jQuery(container_id).addClass('youtube-video');

      jQuery(container_id + ' .jp-play').live('click', function() {
      	if(yt_player.getPlayerState() == 1) {
      	  yt_player.pauseVideo();
      	  jQuery(container_id).removeClass('jp-state-playing');
      	} else {
      	  yt_player.playVideo();
      	  jQuery(container_id).addClass('jp-state-playing');
      	}
      });

      jQuery(container_id + ' .jp-mute').live('click',function() {
      	if(yt_player.isMuted()) {
          yt_player.unMute();
          jQuery(container_id).removeClass('jp-state-muted');
      	} else {
          yt_player.mute();
          jQuery(container_id).addClass('jp-state-muted');
      	}
      });

      jQuery(container_id + ' .jp-volume-bar').click(function(e) {
      	var volume_bar = jQuery(this);
        var posX = volume_bar.offset().left;
        var posWidth = volume_bar.width();
        posX = (e.pageX-posX)/posWidth;
        jQuery(container_id + ' .jp-volume-bar .jp-volume-bar-value').width((posX*100)+'%');
        yt_player.setVolume(posX*100);
      });

      jQuery(container_id + ' .jp-seek-bar').click(function(e) {
      	var seek_bar = jQuery(this);
        var posX = seek_bar.offset().left;
        var posWidth = seek_bar.width();
        posX = (e.pageX-posX)/posWidth;
        jQuery(container_id + ' .jp-progress .jp-play-bar').width((posX*100)+'%');
        posX = Math.round((posX)*duration);
        yt_player.seekTo(posX, true);
        updateOverlays();
      });

      jQuery(container_id + ' .jp-volume-max').live('click', function() {
      	yt_player.setVolume(100);
      	jQuery(container_id + ' .jp-volume-bar .jp-volume-bar-value').width('100%');
      });

      jQuery(container_id + ' .jp-full-screen').live('click',function() {
        if(jQuery(container_id).hasClass('jp-video-full')) {        	
          jQuery(container_id).removeClass('jp-video-full');
          jQuery(container_id + ' .jp-gui').show();
          clearTimeout(fs_hover_time);
          resizeYoutubePlayer();
        } else {        	
          jQuery(container_id).addClass('jp-video-full');
          jQuery(container_id).css({'width':'100%','height':'100%'});
          jQuery(player_id).css({'width':'100%','height':'100%'});
        }
      });
      
      jQuery(container_id + ' .jp-repeat').live('click', function() {
        if(jQuery(container_id).hasClass('jp-state-looped')) {
          yt_player.setLoop(false);
          jQuery(container_id).removeClass('jp-state-looped');
        } else {
          yt_player.setLoop(true);
          jQuery(container_id).addClass('jp-state-looped');
        }
      });

      /*
      var fs_hover_time;
      jQuery(container_id + '.jp-video-full').live('mouseover',function(){
        jQuery(container_id + ' .jp-gui').show();
        clearTimeout(fs_hover_time);
        fullScreenTimeout();
      });

      function fullScreenTimeout(){
        fs_hover_time = setTimeout(function(){
          jQuery(container_id + ' .jp-gui').hide();
        },4000);
      }
      */

      function resizeYoutubePlayer() {
        if(jQuery(container_id).hasClass('jp-video-full')) return;

        var container_width = jQuery('#' + video_id + '-container').parent().innerWidth();
        var container_height = (container_width * 9) / 16;
        var container = jQuery('#' + video_id + '-container');
        var player = jQuery('#' + video_id + '-player');
  
        container.width(container_width + "px");
        container.height(container_height + "px");
        //player.width(container_width + "px");
        //player.height(container_height + "px");        
        player.width("100%");
        player.height(container_height + "px");
      }
      jQuery(window).bind("resize", resizeYoutubePlayer);

      function onPlayerReady(event) {
      	var vol = yt_player.getVolume();
      	jQuery(container_id + ' .jp-volume-bar .jp-volume-bar-value').width(vol + '%');
        resizeYoutubePlayer();
      }

      var youTubeFrequency = 100;
      var youTubeInterval = 0;
      
      function updateSeekBar() {
          var current_time = yt_player.getCurrentTime();
          if(current_time >= 60) {
            jQuery(container_id + ' .jp-current-time').text(Math.floor(current_time/60) + ':' + formatNumberLength(Math.round(current_time % 60), 2));
          } else {
            jQuery(container_id + ' .jp-current-time').text('0:' + formatNumberLength(Math.round(current_time), 2));      
          }
          jQuery(container_id + ' .jp-progress .jp-play-bar').width(Math.round((current_time/duration) * 100) + '%');
      }

      function updateOverlays() {
        var current_time = yt_player.getCurrentTime();

        for(var overlay_key in video_overlays) {
          var overlay = video_overlays[overlay_key];
          var start_time = overlay.start_time;
          var end_time = overlay.end_time;

          if(end_time != -1) {  // If this is not a persistent overlay, check start and end time brackets
            if(current_time >= start_time && current_time < end_time) {
              display_overlay(overlay);
            } else {
              hide_overlay(overlay);
            }
          } else if(current_time >= start_time) {
              display_overlay(overlay);
          } else if(current_time > 0) {
              hide_overlay(overlay);
          }
        }
      }

      function updateYoutubeTime() {
          updateOverlays();
          updateSeekBar();
      }      
     
      function startYoutubeTime() {
          var formatted_duration = '1:00';
          if(duration >= 60) {
            formatted_duration = Math.floor(duration/60) + ':' + formatNumberLength(Math.round(duration % 60), 2);
          } else {
            formatted_duration = '0:' + formatNumberLength(Math.round(duration), 2);
          }
          jQuery(container_id + ' .jp-duration').html(formatted_duration);

          if(youTubeInterval > 0) clearInterval(youTubeInterval);  // stop the interval if one is already running
          youTubeInterval = setInterval(updateYoutubeTime, youTubeFrequency );  // start the interval
      }

      function stopYoutubeTime() {
          if(youTubeInterval > 0) { 
            clearInterval(youTubeInterval);  // stop the interval
            updateSeekBar();
          }          
      }
  
      function formatNumberLength(num,length) {
        var r = "" + num;
        while(r.length < length) { 
          r = "0" + r;
        }
        return r;
      }

      function onPlayerStateChange(event) {
      switch(event.data){
        case -1: //unstarted
          break;
        case YT.PlayerState.ENDED: // ended
          stopYoutubeTime();
          jQuery(container_id).removeClass('jp-state-playing');
          break;
        case YT.PlayerState.PLAYING: // playing
          // explicitly display the controls in case this is mobile; this is to address the issue where mobile youtube controls won't work until the video itself has been interacted with
          jQuery(container_id + ' .jp-gui').show(); 
          duration = yt_player.getDuration();          
          jQuery(container_id).addClass('jp-state-playing');
          startYoutubeTime();
          break;
        case YT.PlayerState.PAUSED: // paused
          jQuery(container_id).removeClass('jp-state-playing');
          stopYoutubeTime();
          break;
        case YT.PlayerState.BUFFERING: // buffering
          break;
        case YT.PlayerState.CUED: // video cued
          break;
        default:
          break;
        }
      }
    }