<?php

/**
 * @file
 * Field definition for HubDub overlays.
 */

/**
 * Implements hook_field_info().
 */
function hubdub_overlay_field_field_info() {
  return array(
    'hubdub_overlay' => array(
      'label' => t('HubDub Overlay'),
      'description' => t('This field stores markup to overlay on top of the video at a predetermined time.'),
      'default_widget' => 'hubdub_overlay_field',
      'default_formatter' => 'hubdub_default',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function hubdub_overlay_field_field_widget_info() {
  return array(
    'hubdub_overlay_field' => array(
      'label' => t('Text field'),
      'field types' => array('hubdub_overlay'),
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function hubdub_overlay_field_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];
  $form = array();
  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function hubdub_overlay_field_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  $form = array();
  return $form;
}

/**
 * Implements hook_field_widget_form().
 *
 * TODO: Make individual values deletable.
 * See http://www.cleancode.co.nz/blog/1131/multi-values-field-development-drupal?
 */
function hubdub_overlay_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'hubdub_overlay_field':
      $element['hubdub_overlay'] = array(
        '#type' => 'fieldset',
        '#title' => '',
        '#tree' => TRUE,
      );
      // Don't collapse the fieldset if it's a new value.
      if (isset($items[$delta]['title'])) {
        $element['hubdub_overlay']['#title'] = $items[$delta]['title'];
        $element['hubdub_overlay']['#collapsible'] = TRUE;
        $element['hubdub_overlay']['#collapsed'] = TRUE;
      }
      $element['hubdub_overlay']['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => isset($items[$delta]['title']) ? $items[$delta]['title'] : '',
        '#size' => 64,
        '#attributes' => array('maxlength' => 64),
      );
      $element['hubdub_overlay']['start_time'] = array(
        '#type' => 'textfield',
        '#title' => t('Start Time'),
        '#default_value' => isset($items[$delta]['start_time']) ? $items[$delta]['start_time'] : '',
        '#description' => t('Due to limitations of jPlayer, an exact time cannot be guaranteed.'),
        '#element_validate' => array('hubdub_overlay_start_time_validate'),
      );
      $element['hubdub_overlay']['end_time'] = array(
        '#type' => 'textfield',
        '#title' => t('End Time'),
        '#default_value' => isset($items[$delta]['end_time']) ? $items[$delta]['end_time'] : '',
        '#description' => t('Due to limitations of jPlayer, an exact time cannot be guaranteed. If you want this overlay to persist past the end of the video, enter a value of -1 here.'),
        '#element_validate' => array('hubdub_overlay_end_time_validate'),
      );
      $element['hubdub_overlay']['markup'] = array(
        '#type' => 'text_format',
        '#base_type' => 'textarea',
        '#title' => t('Markup'),
        '#default_value' => isset($items[$delta]['markup']) ? $items[$delta]['markup'] : '',
        '#format' => isset($items[$delta]['format']) ? $items[$delta]['format'] : 'filtered_html',
      );
      if (isset($items[$delta]['data'])) {
        $data = unserialize($items[$delta]['data']);
      }
      $element['hubdub_overlay']['data']['pause_on_focus'] = array(
        '#type' => 'checkbox',
        '#title' => t('Pause on focus?'),
        '#default_value' => isset($data['pause_on_focus']) ? $data['pause_on_focus'] : FALSE,
        '#description' => t('Pause the video when this overlay receives focus?'),
      );
      break;
  }
  return $element;
}

/**
 * Implements hook_field_presave().
 */
function hubdub_overlay_field_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    if (isset($item['hubdub_overlay']['markup'])) {
      $items[$delta]['title'] = $item['hubdub_overlay']['title'];
      $items[$delta]['start_time'] = $item['hubdub_overlay']['start_time'];
      $items[$delta]['end_time'] = $item['hubdub_overlay']['end_time'];
      $items[$delta]['markup'] = $item['hubdub_overlay']['markup']['value'];
      $items[$delta]['format'] = $item['hubdub_overlay']['markup']['format'];
      $items[$delta]['data'] = serialize($item['hubdub_overlay']['data']);
      // $items[$delta]['settings'] = array('key' => 'test');
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function hubdub_overlay_field_field_is_empty($item, $field) {
  if (empty($item['hubdub_overlay']['title'])) {
    return TRUE;
  }
  elseif (!is_numeric($item['hubdub_overlay']['start_time'])) {
    return TRUE;
  }
  elseif (!is_numeric($item['hubdub_overlay']['end_time'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_validate().
 */
function hubdub_overlay_field_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // Loop through field items in the case of multiple values.
  foreach ($items as $delta => $item) {
    // TODO: Validate HTML in the markup field.
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function hubdub_overlay_field_field_formatter_info() {
  return array(
    'hubdub_overlay_default' => array(
      'label' => t('Default'),
      'field types' => array('hubdub_video'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function hubdub_overlay_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'hubdub_default':
      foreach ($items as $delta => $item) {
        if (isset($item['markup'])) {
          $element[$delta]['#markup'] = check_plain($item['title']) . ' ' . $item['markup'];
        }
      }
      break;
  }
  return $element;
}

/**
 * Validate start times of the overlay fields.
 */
function hubdub_overlay_start_time_validate($element, &$form_state, $form) {
  // Skip validation if it is empty; this indicates overlay is to be removed.
  if (!empty($element['#value'])) {
    // Check that the value is a number.
    if (!is_numeric($element['#value'])) {
      form_set_error($element['#name'], t('The input for the %fieldname field is not valid. It must be an integer.', array('%fieldname' => $element['#title'])));
    }
    // Check that the value is greater than or equal to 0
    if ($element['#value'] < 0) {
      form_set_error($element['#name'], t('The input for the %fieldname field is not valid. It must be greater than or equal to 0.', array('%fieldname' => $element['#title'])));
    }
  }
}

/**
 * Validate end times of the overlay fields.
 */
function hubdub_overlay_end_time_validate($element, &$form_state, $form) {
  // Skip validation if it is empty; this indicates overlay is to be removed.
  if (!empty($element['#value'])) {
    // Check that the value is a number and that the value is greater than or
    // equal to -1; -1 is a valid value for end time.
    if (!is_numeric($element['#value']) || $element['#value'] < -1) {
      form_set_error($element['#name'], t('The input for the %fieldname field is not valid. It must be an integer greater than or equal to -1.', array('%fieldname' => $element['#title'])));
    }
  }
}
