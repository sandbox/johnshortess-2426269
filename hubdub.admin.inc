<?php
/**
 * @file
 * HubDub Video editing UI.
 */

/**
 * UI controller.
 */
class HubdubVideoUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage HubDub videos.';
    return $items;
  }

}


/**
 * Form definition for adding / editing a video.
 */
function hubdub_video_form($form, &$form_state, $hubdub_video = NULL, $op = 'edit') {
  if ($op == 'clone') {
    // Only label is provided for cloned entities.
    $hubdub_video->title .= ' (cloned)';
  }

  $form['title'] = array(
    '#title' => t('Video title'),
    '#type' => 'textfield',
    '#default_value' => isset($hubdub_video->title) ? $hubdub_video->title : '',
    '#required' => TRUE,
    '#weight' => 1,
  );

  $form['name'] = array(
    '#title' => t('Video machine name'),
    '#type' => 'machine_name',
    '#default_value' => isset($hubdub_video->name) ? $hubdub_video->name : '',
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'hubdub_get_videos',
      'source' => array('title'),
    ),
    '#description' => t('A unique machine-readable name for this video. It must only contain lowercase letters, numbers, and underscores.'),
    '#weight' => 2,
  );

  $form['description'] = array(
    '#title' => t('Video description'),
    '#type' => 'textarea',
    '#default_value' => isset($hubdub_video->description) ? $hubdub_video->description : '',
    '#weight' => 3,
  );

  $form['url'] = array(
    '#title' => t('Video URL'),
    '#type' => 'textfield',
    '#default_value' => isset($hubdub_video->url) ? $hubdub_video->url : '',
    '#required' => TRUE,
    '#description' => t('This MUST be the direct URL to a m4v (.mp4) file or RTMP stream.'),
    '#weight' => 4,
  );

  $form['skin'] = array(
    '#title' => t('Player Skin'),
    '#type' => 'select',
    '#options' => array(
      'default' => t('Default'),
      'bright' => t('Bright'),
      'inverse' => t('Inverse'),
    ),
    '#default_value' => isset($hubdub_video->data['skin']) ? $hubdub_video->data['skin'] : 'default',
    '#weight' => 5,
  );

  // Only ask if GA tracking should be enabled if GA module is enabled.
  if (module_exists('googleanalytics')) {
    $form['data']['ga_enable'] = array(
      '#title' => t('Enable Google Analytics tracking'),
      '#type' => 'checkbox',
      '#default_value' => isset($hubdub_video->data['ga_enable']) ? $hubdub_video->data['ga_enable'] : FALSE,
      '#weight' => 6,
    );
  }

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Settings'),
    '#weight' => 10,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['loop'] = array(
    '#title' => t('Loop video?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['loop']) ? $hubdub_video->data['loop'] : FALSE,
    '#weight' => 11,
  );

  $form['advanced']['autostart'] = array(
    '#title' => t('Autostart video?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['autostart']) ? $hubdub_video->data['autostart'] : FALSE,
    '#weight' => 11,
  );

  $form['advanced']['preload'] = array(
    '#title' => t('Preload video?'),
    '#type' => 'select',
    '#options' => array(
      'none' => t('No'),
      'metadata' => t('Preload metadata only'),
      'auto' => t('Yes'),
    ),
    '#default_value' => isset($hubdub_video->data['preload']) ? $hubdub_video->data['preload'] : 'metadata',
    '#weight' => 12,
  );

  $form['advanced']['smoothPlayBar'] = array(
    '#title' => t('Smooth play bar transitions?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['smoothPlayBar']) ? $hubdub_video->data['smoothPlayBar'] : FALSE,
    '#weight' => 13,
  );

  $form['advanced']['autoBlur'] = array(
    '#title' => t('GUI interactions blur() after executing?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['autoBlur']) ? $hubdub_video->data['autoBlur'] : FALSE,
    '#weight' => 14,
  );

  $form['advanced']['backgroundColor'] = array(
    '#title' => t('Player background color'),
    '#type' => 'textfield',
    '#size' => 7,
    '#maxlength' => 7,
    '#default_value' => isset($hubdub_video->data['backgroundColor']) ? $hubdub_video->data['backgroundColor'] : '#000000',
    '#weight' => 15,
  );

  $form['advanced']['keyEnabled'] = array(
    '#title' => t('Enable keyboard controls?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['keyEnabled']) ? $hubdub_video->data['keyEnabled'] : FALSE,
    '#weight' => 16,
  );

  $form['advanced']['errorAlerts'] = array(
    '#title' => t('Enable error alerts?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['errorAlerts']) ? $hubdub_video->data['errorAlerts'] : FALSE,
    '#weight' => 17,
  );

  $form['advanced']['warningAlerts'] = array(
    '#title' => t('Enable warning alerts?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['warningAlerts']) ? $hubdub_video->data['warningAlerts'] : FALSE,
    '#weight' => 18,
  );

  $form['advanced']['consoleAlerts'] = array(
    '#title' => t('Write error and warning alerts to console instead?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['consoleAlerts']) ? $hubdub_video->data['consoleAlerts'] : FALSE,
    '#weight' => 19,
  );

  $form['advanced']['autohide'] = array(
    '#type' => 'fieldset',
    '#title' => t('AutoHide Settings'),
    '#weight' => 20,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['autohide']['restored'] = array(
    '#title' => t('Auto-hide the GUI when in restored screen display state?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['autoHide']['restored']) ? $hubdub_video->data['autoHide']['restored'] : FALSE,
    '#weight' => 21,
  );

  $form['advanced']['autohide']['full'] = array(
    '#title' => t('Auto-hide the GUI when in full screen display state?'),
    '#type' => 'checkbox',
    '#default_value' => isset($hubdub_video->data['autoHide']['full']) ? $hubdub_video->data['autoHide']['full'] : TRUE,
    '#weight' => 22,
  );

  $form['advanced']['autohide']['fadeIn'] = array(
    '#title' => t('Length of fadeIn animation?'),
    '#type' => 'textfield',
    '#field_suffix' => 'milliseconds',
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => isset($hubdub_video->data['autoHide']['fadeIn']) ? $hubdub_video->data['autoHide']['fadeIn'] : 200,
    '#element_validate' => array('element_validate_integer_positive'),
    '#weight' => 23,
  );

  $form['advanced']['autohide']['fadeOut'] = array(
    '#title' => t('Length of fadeOut animation?'),
    '#type' => 'textfield',
    '#field_suffix' => 'milliseconds',
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => isset($hubdub_video->data['autoHide']['fadeOut']) ? $hubdub_video->data['autoHide']['fadeOut'] : 600,
    '#element_validate' => array('element_validate_integer_positive'),
    '#weight' => 24,
  );

  $form['advanced']['autohide']['hold'] = array(
    '#title' => t('Delay before autoHide begins?'),
    '#type' => 'textfield',
    '#field_suffix' => 'milliseconds',
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => isset($hubdub_video->data['autoHide']['hold']) ? $hubdub_video->data['autoHide']['hold'] : 1000,
    '#element_validate' => array('element_validate_integer_positive'),
    '#weight' => 25,
  );

  $form['overlay'] = array(
    '#weight' => 40,
  );

  field_attach_form('hubdub_video', $hubdub_video, $form['overlay'], $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save video'),
    '#weight' => 50,
  );
  return $form;
}

/**
 * Submit handler for the video add/edit form.
 */
function hubdub_video_form_submit($form, &$form_state) {
  if (isset($form_state['values']['skin'])) {
    $form_state['values']['data']['skin'] = $form_state['values']['skin'];
    unset($form_state['values']['skin']);
  }
  if (isset($form_state['values']['ga_enable'])) {
    $form_state['values']['data']['ga_enable'] = $form_state['values']['ga_enable'];
    unset($form_state['values']['ga_enable']);
  }
  if (isset($form_state['values']['loop'])) {
    $form_state['values']['data']['loop'] = $form_state['values']['loop'];
    unset($form_state['values']['loop']);
  }
  if (isset($form_state['values']['autostart'])) {
    $form_state['values']['data']['autostart'] = $form_state['values']['autostart'];
    unset($form_state['values']['autostart']);
  }
  if (isset($form_state['values']['preload'])) {
    $form_state['values']['data']['preload'] = $form_state['values']['preload'];
    unset($form_state['values']['preload']);
  }
  if (isset($form_state['values']['smoothPlayBar'])) {
    $form_state['values']['data']['smoothPlayBar'] = $form_state['values']['smoothPlayBar'];
    unset($form_state['values']['smoothPlayBar']);
  }
  if (isset($form_state['values']['autoBlur'])) {
    $form_state['values']['data']['autoBlur'] = $form_state['values']['autoBlur'];
    unset($form_state['values']['autoBlur']);
  }
  if (isset($form_state['values']['backgroundColor'])) {
    $form_state['values']['data']['backgroundColor'] = $form_state['values']['backgroundColor'];
    unset($form_state['values']['backgroundColor']);
  }
  if (isset($form_state['values']['keyEnabled'])) {
    $form_state['values']['data']['keyEnabled'] = $form_state['values']['keyEnabled'];
    unset($form_state['values']['keyEnabled']);
  }
  if (isset($form_state['values']['errorAlerts'])) {
    $form_state['values']['data']['errorAlerts'] = $form_state['values']['errorAlerts'];
    unset($form_state['values']['errorAlerts']);
  }
  if (isset($form_state['values']['warningAlerts'])) {
    $form_state['values']['data']['warningAlerts'] = $form_state['values']['warningAlerts'];
    unset($form_state['values']['warningAlerts']);
  }
  if (isset($form_state['values']['consoleAlerts'])) {
    $form_state['values']['data']['consoleAlerts'] = $form_state['values']['consoleAlerts'];
    unset($form_state['values']['consoleAlerts']);
  }
  if (isset($form_state['values']['restored'])) {
    $form_state['values']['data']['autoHide']['restored'] = $form_state['values']['restored'];
  }
  if (isset($form_state['values']['full'])) {
    $form_state['values']['data']['autoHide']['full'] = $form_state['values']['full'];
  }
  if (isset($form_state['values']['fadeIn'])) {
    $form_state['values']['data']['autoHide']['fadeIn'] = $form_state['values']['fadeIn'];
  }
  if (isset($form_state['values']['fadeOut'])) {
    $form_state['values']['data']['autoHide']['fadeOut'] = $form_state['values']['fadeOut'];
  }
  if (isset($form_state['values']['hold'])) {
    $form_state['values']['data']['autoHide']['hold'] = $form_state['values']['hold'];
  }

  $hubdub_video = entity_ui_form_submit_build_entity($form, $form_state);
  $hubdub_video->save();
  drupal_set_message(t('The video @name has been saved.', array('@name' => $hubdub_video->title)));
  $form_state['redirect'] = 'admin/structure/hubdub_videos';
}

/**
 * @TODO: Document this.
 */
function hubdub_video_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'])) {
    form_set_error('url', t('Not a valid URL.'));
  }

  if (!preg_match('/^#([a-f0-9]{3}){1,2}$/iD', $form_state['values']['backgroundColor'])) {
    form_set_error('backgroundColor', t('Not a valid hex CSS color triplet'));
  }
}
