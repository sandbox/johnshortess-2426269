<?php

/**
 * @file
 * Field definition for HubDub overlays.
 */

/**
 * Implements hook_field_info().
 */
function hubdub_field_info() {
  return array(
    'hubdub_overlay' => array(
      'label' => t('Overlay'),
      'description' => t('This field stores markup to overlay on top of the video at a predetermined time.'),
      'default_widget' => 'hubdub_overlay_field',
      'default_formatter' => 'hubdub_default',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function hubdub_field_widget_info() {
  return array(
    'hubdub_overlay_field' => array(
      'label' => t('Text field'),
      'field types' => array('hubdub_overlay'),
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function hubdub_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];
  $form = array();
  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function hubdub_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  $form = array();
  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function license_plate_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'hubdub_overlay_field':
      $element['hubdub_overlay'] = array(
        '#type' => 'fieldset',
        '#title' => check_plain($element['#title']),
        '#tree' => TRUE,
      );
      $element['hubdub_overlay']['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => isset($items[$delta]['title']) ? $items[$delta]['title'] : '',
        '#required' => $element['#required'],
        '#size' => 64,
        '#attributes' => array('maxlength' => 64),
      );
      $element['hubdub_overlay']['name'] = array(
        '#type' => 'machine_name',
        '#title' => t('Machine Name'),
        '#default_value' => isset($items[$delta]['machine_name']) ? $items[$delta]['machine_name'] : '',
        '#size' => 64,
        '#required' => $element['#required'],
      );
      $element['hubdub_overlay']['start_time'] = array(
        '#type' => 'float',
        '#title' => t('Start Time'),
        '#default_value' => isset($items[$delta]['start_time']) ? $items[$delta]['start_time'] : '0.0',
      );
      $element['hubdub_overlay']['end_time'] = array(
        '#type' => 'float',
        '#title' => t('End Time'),
        '#default_value' => isset($items[$delta]['end_time']) ? $items[$delta]['end_time'] : '0.0',
      );
      $element['hubdub_overlay']['markup'] = array(
        '#type' => 'textarea',
        '#title' => t('Markup'),
        '#default_value' => isset($items[$delta]['markup']) ? $items[$delta]['markup'] : '',
      );
      break;
  }
  return $element;
}

/**
 * Implements hook_field_presave().
 */
function hubdub_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $item) {
    if (isset($item['hubdub_overlay']['markup'])) {
      $items[$delta]['title'] = $item['hubdub_overlay']['title'];
      $items[$delta]['name'] = $item['hubdub_overlay']['name'];
      $items[$delta]['start_time'] = $item['hubdub_overlay']['start_time'];
      $items[$delta]['end_time'] = $item['hubdub_overlay']['end_time'];
      $items[$delta]['overlay'] = $item['hubdub_overlay']['overlay'];
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function hubdub_field_is_empty($item, $field) {
  if (empty($item['hubdub_overlay']['title'])) {
    return TRUE;
  }
  elseif (empty($item['hubdub_overlay']['name'])) {
    return TRUE;
  }
  elseif (empty($item['hubdub_overlay']['start_time'])) {
    return TRUE;
  }
  elseif (empty($item['hubdub_overlay']['end_time'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_validate().
 */
function hubdub_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // Loop through field items in the case of multiple values.
  foreach ($items as $delta => $item) {
    // TODO: Add any validation code here.
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function hubdub_field_formatter_info() {
  return array(
    'hubdub_overlay_default' => array(
      'label' => t('Default'),
      'field types' => array('hubdub_video'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function hubdub_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'license_plate_default':
      foreach ($items as $delta => $item) {
        if (isset($item['plate_number'])) {
          $element[$delta]['#markup'] = $item['state'] . ' ' . $item['plate_number'];
        }
      }
      break;
  }
  return $element;
}
