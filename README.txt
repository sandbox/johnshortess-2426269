CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Use
 * Customization/Theming
 * Known Issues
 * For More Information


INTRODUCTION
------------

The HubDub module enables the creation of interactive videos, with HTML overlays
displayed over or around the video at predetermined times during playback. The
original design use-case was for nonprofits to make a call to action in a video,
then let viewers perform that action (such as submitting a CRM signup, online
petition, or donation form) from directly within the video player. The module
can also be used to create interactive captions (such as lower-thirds or live
tickers), branching videos, etc.

Videos and their associated overlays and other metadata are stored in a custom
entity type, and exposed as a block with an embedded jPlayer video player.
Currently self-hosted or Vimeo MP4 videos are supported, but wider support
(including YouTube) is planned.


REQUIREMENTS
------------

HubDub requires Entity API, Libraries API, and the jPlayer library
(http://jplayer.org/)


INSTALLATION
------------

Install HubDub  and the HubDub Overlay Field submodule as you would any other
Drupal module. If you need help, see
http://drupal.org/documentation/install/modules-themes/modules-7.

Once you've installed the module, download the jPlayer library from
http://jplayer.org/download/ and install in /sites/all/libraries, so that the
jquery.jplayer.min.js file is at
/sites/all/libraries/jPlayer/jplayer/jquery.jplayer.min.js.


CONFIGURATION
-------------

There is currently no configuration required other than granting appropraite
access to the "Administer HubDub" permission. Note that in order to add
Javascript to Hubdub overlays, a user must have access to the "Full HTML" text
format. This has security implications, so use discretion when granting this
access.


USE
---

Add or edit HubDub videos at /admin/structure/hubdub_videos. Some notes/caveats:

* The Video URL field must be the actual location of a .m4v file. Support for
  other file formats and parsing of YouTube/Vimeo URLs is planned.
  
* Due to limitations of jPlayer, exact overlay start and end times cannot be
  guaranteed. If your overlays need to be frame-accurate, this probably isn't
  the solution for you.
  
* The Overlay Markup field defaults to the Filtered HTML text format. If you
  wish to include Javascript in your overlays, you must use the Full HTML
  format. Note that all Javascript for all overlays will be executed immediately
  when the page loads. If you need to sync a script to a specific overlay,
  you'll probably want to set up a jQuery event handler that triggers when that
  overlay becomes visible.
  
HubDub automatically creates a block for each video, which you can place as
desired.


CUSTOMIZATION/THEMING
---------------------

See THEMING.txt.


KNOWN ISSUES
------------

* The "poster" placeholder image for the video player does not display in
  mobile.
  

FOR MORE INFORMATION
--------------------

 * Project Page: http://www.drupal.org/sandbox/johnshortess/2426269
 * Issue Queue: http://www.drupal.org/project/issues/2426269
 * Documentation: Coming soon.
 * Demo: http://www.4sitestudios.com
