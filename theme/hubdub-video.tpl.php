<?php
/**
 * @file hubdub-video.tpl.php
 * Displays an embedded video player for HubDub videos.
 *
 * Available variables:
 *   - $name: Machine name of the video to display
 *   - $overlays: An array of overlays to display with the video.
 *
 * @see hubdub_preprocess_hubdub_video()
 *
 * @ingroup themeable
 *
 * @todo: Split overlays into a separate field template, and iterate through the
 * overlays array in a field preprocess function.
 */
?>

<div class="hubdub-video jp-video" id="hd_<?php print $name; ?>-container" role="application" aria-label="media player">
  <div class="jp-type-single">
      <div class="video-and-overlay-container">
          <div class="jp-gui-play-icon">
              <div class="jp-video-play"><button class="jp-video-play-icon" role="button" tabindex="0">play</button></div>
              </div>
   <div class="jp-jplayer" id="hd_<?php print $name; ?>-player"></div>
      <?php
      foreach ($overlays as $id => $overlay): ?>
	  <div id="<?php print $id; ?>" class="hubdub-overlay<?php if(unserialize($overlay['data'])['pause_on_focus']): echo ' pause-on-focus'; ?>" style="display: none;"><?php print html_entity_decode(check_markup($overlay['markup'], $overlay['format']), ENT_QUOTES); ?></div>
      </div>
   <div class="jp-gui">
    <div class="jp-interface">
        <div class="jp-topbar">
            <button class="jp-play" role="button" tabindex="0">play</button>
            <!-- <button class="jp-stop" role="button" tabindex="0">stop</button> -->
            <div class="jp-progress">
                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                <div class="jp-title" aria-label="title">&nbsp;</div>
                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                <div class="jp-seek-bar"><div class="jp-play-bar"></div></div>
            </div>
            <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
        </div>
        <div class="jp-controls-holder">
          <div class="jp-volume-controls">
            <button class="jp-mute" role="button" tabindex="0">mute</button>
            <div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
          </div>
          <div class="jp-toggles">
            <button class="jp-repeat" role="button" tabindex="0">repeat</button>

          </div>
            <div class="jp-player-clear"></div>
        </div>
      </div>
    </div>
    <div class="jp-no-solution"><span>Update Required</span>To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div>
  </div>

</div>
